<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('/nuvem', function () {
    return view('welcome');
});

Route::get('/upload', 'FileController@view');
Route::get('/gallery', 'FileController@gallery');
Route::get('/files', 'FileController@allFiles');
Route::get('/show/{filename}', 'FileController@show');
Route::get('/download/{filename}', 'FileController@downloadFile')->name('download');

Route::post('/upload', 'FileController@uploadFile')->name('uploadFile');

Route::get('/login', 'LoginController@form');
Route::post('login', 'LoginController@login')->name('login');

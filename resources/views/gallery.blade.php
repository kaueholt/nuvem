<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <title>Galeria de arquivos</title>
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="title" style="margin:0 20px 0 20px">
                Download
            </div>
            <span style="font-size: 3rem;">
                <span style="color: Mediumslateblue;">
                    <i class="fas fa-cloud-download-alt"></i>
                </span>
            </span>
        </div>
    </div>
    <!-- Grid row -->
    <div class="row">

        <!-- Grid column -->
        <div id="Filters" class="col-md-12 d-flex justify-content-center mb-5">
            <button type="button" onclick="location.href = '/nuvem'" class="btn btn-outline-black waves-effect" name="home">
                <i class="fas fa-home"></i> Início
            </button>
            <button type="button" class="btn btn-outline-black waves-effect filter active" data-rel="all">All</button>
            <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="1">Imagens</button>
            <button type="button" class="btn btn-outline-black waves-effect filter" data-rel="2">PDF's</button>
            <button type="button" onclick="location.href = '/upload'" class="btn btn-outline-black waves-effect" name="upload">
                <i class="fas fa-cloud-upload-alt"></i> Upload
            </button>

        </div>
    </div>

    <div class="gallery" id="gallery">



        <?php
        $dir = new DirectoryIterator(public_path() . '/storage');

        foreach ($dir as $fileinfo) {
            // echo dd($fileinfo);
            // echo gettype($fileinfo);
            if (
                $fileinfo->getExtension() == 'png' ||
                $fileinfo->getExtension() == 'jpg' ||
                $fileinfo->getExtension() == 'gif'
            ) {

                $path = 'storage/' . $fileinfo->getFilename();
                $filename = $fileinfo->getFilename();
                echo "<div class='mb-4 pics animation all 1'>";
                echo "<img class='img-fluid' src='$path'
                    onclick='download(\"$filename\")'
                        style='max-height:200px;
                        max-width:250px'
                        border:1px solid black'><br>
                        </img>
                        </div>";
            }
            else if ($fileinfo->getExtension() == 'pdf') {

                $path = 'storage/' . $fileinfo->getFilename();
                $filename = $fileinfo->getFilename();
                echo "<div class='mb-4 pics animation all 2'>";
                echo "<embed
                    width='320'
                    height='250'
                    name=$filename
                    src=$path
                    type='application/pdf'/>
                </div>";
            }
        }
        ?>
    </div>
</body>


<script>
    $(function() {
        var selectedClass = "";
        $(".filter").click(function() {
            selectedClass = $(this).attr("data-rel");
            $("#gallery").fadeTo(100, 0.1);
            $("#gallery div").not("." + selectedClass).fadeOut().removeClass('animation');
            setTimeout(function() {
                $("." + selectedClass).fadeIn().addClass('animation');
                $("#gallery").fadeTo(300, 1);
            }, 300);
        });
    });

    function download(path) {
        let url = "{{ route('download', ':path') }}";
        url = url.replace(':path', path);
        document.location.href = url;
    }


    var btnContainer = document.getElementById("Filters");

    // Get all buttons with class="btn" inside the container
    var btns = btnContainer.getElementsByClassName("btn");

    // Loop through the buttons and add the active class to the current/clicked button
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function() {
            var current = document.getElementsByClassName("active");
            current[0].className = current[0].className.replace(" active", "");
            this.className += " active";
        });
    }
</script>


<style>
    .btn {
        margin: 10px 0px -40px 10px
    }

    .gallery {
        -webkit-column-count: 4;
        -moz-column-count: 4;
        column-count: 4;
        -webkit-column-width: 25%;
        -moz-column-width: 25%;
        column-width: 25%;
    }

    .gallery .pics {
        -webkit-transition: all 350ms ease;
        transition: all 350ms ease;
    }

    .gallery .animation {
        -webkit-transform: scale(1);
        -ms-transform: scale(1);
        transform: scale(1);
    }

    @media (max-width: 450px) {
        .gallery {
            -webkit-column-count: 1;
            -moz-column-count: 1;
            column-count: 1;
            -webkit-column-width: 100%;
            -moz-column-width: 100%;
            column-width: 100%;
        }
    }

    @media (max-width: 400px) {
        .btn.filter {
            padding-left: 1.1rem;
            padding-right: 1.1rem;
        }
    }

    .title {
        font-size: 72px;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 100;
        margin: 0;
    }

    .active {
        border-bottom: 1px solid black;
    }
</style>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

    <title>Upload de arquivos</title>
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="title" style="margin:0 20px 0 20px">
                Upload
            </div>
            <span style="font-size: 3rem;">
                <span style="color: Mediumslateblue;">
                    <i class="fas fa-cloud-upload-alt"></i>
                </span>
            </span>
        </div>
        <div class="row justify-content-md-center" style="margin-top:40px">
            <div class="col col-lg-6">
                <form method="POST" action="{{ route('uploadFile') }}" enctype="multipart/form-data">
                    <div class="custom-file">
                        <label class="custom-file-label" for="file" id="labelFile">Escolha o arquivo</label>
                        <input type="file" class="custom-file-input" id="file" name="file" />
                        </br>
                        </br>
                    </div>

                    <!-- Laravel has CSRF protection out of the box
                    To make use of it (and make the error go away) add a hidden field to your form: -->
                    <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                    <div class="col-md-12 d-flex justify-content-center mb-5">
                        <button type="button" onclick="location.href = '/nuvem'" class="btn btn-outline-black waves-effect" name="home">
                            <i class="fas fa-home"></i> Início
                        </button>
                        <button type="submit" class="btn btn-primary col-md-4" disabled id="submitFile" name="submit" style="margin: 0 40px 0 40px">Enviar</button>
                        <button type="button" onclick="location.href = '/gallery'" class="btn btn-outline-black waves-effect" name="download">
                            <i class="fas fa-cloud-download-alt"></i> Download
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</body>
<script>
    jQuery.fn.extend({
        disable: function(state) {
            return this.each(function() {
                this.disabled = state;
            });
        }
    });
    $("#file").change(function() {
        $("#labelFile").text(this.files[0].name);
        $('#submitFile').prop('disabled', false);
    });
</script>
<style>
    .title {
        font-size: 72px;
        color: #636b6f;
        font-family: 'Nunito', sans-serif;
        font-weight: 100;
        margin: 0;
    }
</style>

</html>
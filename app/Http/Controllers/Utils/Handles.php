<?php

namespace App\Http\Controllers\Utils;
use Illuminate\Http\Request;

class Handles
{
    private $model;

    function __construct($model){
        $this->model = $model;
    }

    /**
    * Undocumented function
    *
    * @param [type] $this->model
    * @param [type] $data
    * @return void
    */
    public function handleList($data)
    {
        $message = $data ? 'Listagem realizada com sucesso' : 'Erro ao retornar lista de items';
        return Handles::jsonResponse(false, 'success', $message, $data);
    }
    /**
    * Undocumented function
    *
    * @param [type] $this->model
    * @param Request $request
    * @return void
    */
    public function handleStore( $payload)
    {
        $response = null;
        try {
            $data = $this->model::create($payload);
            $message = $data ? 'Inserção realizada com sucesso' : 'Erro ao retornar lista de items';
            $response =  Handles::jsonResponse(false, 'success', $message, $data);
        } catch (\PDOException $e) {
             switch ($e->getCode()) {
                case 23000: // Registro relacionado
                $message = 'O item que está tentando inserir já está cadastrado na base de dados.';
                $response = Handles::jsonResponse(true, 'error', $message, null);
                break;
            }
        }
        return $response;
    }
    /**
    * Undocumented function
    *
    * @param [type] $this->model
    * @param [type] $id
    * @return void
    */
    public function handleShow( $id)
    {
        $data = $this->model::where('id', $id)->first();
        $message = $data ? 'Item retornado com sucesso!' : "Erro ao retornar o item: {$id}";
        return Handles::jsonResponse(false, 'success', $message, $data);
    }

    /**
    * Undocumented function
    *
    * @param [type] $id
    * @param [type] $payload
    * @return void
    */
    public function handleUpdate( $id, $payload)
    {
        $data =  $this->model::where('id', $id)->update($payload);
        $message = $data ? 'Edição realizada com sucesso' : 'Erro ao retornar lista de items';
        return Handles::jsonResponse(false, 'success', $message, $payload);
    }
    /**
    * Undocumented function
    *
    * @param [type] $id
    * @return void
    */
    public function handleDelete( $id)
    {
        $response = null;
        try {
            $data = $this->model::where('id', $id)->delete();
            $response = Handles::jsonResponse(false, 'success', 'Item removido com sucesso!', $data);
        } catch (\PDOException $e) {
            switch ($e->getCode()) {
                case 23000: // Registro relacionado
                $message = 'Existem dados relacionados com esse registro.Remova-os antes de deletar este item';
                $response = Handles::jsonResponse(true, 'error', $message, null);
                break;
            }
        }
        return $response;
    }

    /**
    * Undocumented function
    *
    * @param [type] $err
    * @param string $type
    * @param string $message
    * @param [type] $data
    * @return void
    */
    public static function jsonResponse($success, string $message, $data = null, $status=200)
    {
        return response()->json(
            array(
                'success' => $success,
                'message' => $message,
                'data' => $data
            ), $status
        );
    }

}

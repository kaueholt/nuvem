<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;

class LoginController extends Controller
{
    public function form(Request $request){
        return view('login');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('login','senha');
        return $credentials['login'] == 'nuvem' && 
                $credentials['senha'] == 'pipa'
            ?   view('welcome')
            :   view('login');
    }

}

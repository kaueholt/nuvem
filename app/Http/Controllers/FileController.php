<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function view(){
        return view('upload');
    }

    public function gallery(){
        return view('gallery');
    }

    public function uploadFile(Request $request){
        $request->file->store('public');
        return view('welcome');
    }

    public function allFiles(){
        return Storage::allFiles('public');
    }
    public function show($filename){
        // return view(Storage::get('public\CpQGcxBs9EbM1LhHNE8cQpF4fu3Zt8RU39XNnCEg.png'));
        return asset('storage/CpQGcxBs9EbM1LhHNE8cQpF4fu3Zt8RU39XNnCEg.png');
    }

    public function downloadFile($filename){
        // return Storage::download("public\.$filename");
        return Storage::download('public/'.$filename);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Utils\Handles;

class DefaultController extends Controller
{
    public function getAll(Request $request, $table)
    {
        $model = "App\\Models\\" . $table;
        $response = $model::all();
        return $response
            ?   Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            :   Handles::jsonResponse(false, 'Nenhum registro encontrado', [], 200);
    }

    public function getOne($table, $id)
    {
        $model = "App\\Models\\" . $table;
        $response = $model::find($id);
        return $response
            ?   Handles::jsonResponse(true, 'Registro encontrado!', $response, 200)
            :   Handles::jsonResponse(false, 'Registro não encontrado!', [], 200);
    }

    public function store(Request $request, $table)
    {
        $model = "App\\Models\\" . $table;
        $payload = $request->all();
        try {
            $query = new $model();
            //verifica se está sendo passada a chave primária, se estiver, verifica se já não existe registro com esta chave
            $chavePrimariaJaExiste = isset($payload[$query->primaryKey])
                ? $model::find($payload[$query->primaryKey])
                : null;
            if ($chavePrimariaJaExiste)
                return Handles::jsonResponse(
                    false,
                    'Registro não inserido - Chave ' . $query->primaryKey . '->\'' . $payload[$query->primaryKey] . '\' já existe!',
                    $chavePrimariaJaExiste,
                    406
                );

            $query->fill($payload);
            if (!$query->save())
                return Handles::jsonResponse(true, 'Falha ao inserir!', $query, 406);

            return Handles::jsonResponse(true, 'Registro inserido! Tabela ' . $query->table . ', ' . $query->primaryKey . ' ' . $query->getKey(), $query, 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse(false, 'Registro não inserido - Dados faltantes!', $exception, 406);
        } catch (\InvalidArgumentException $exception) {
            return Handles::jsonResponse(false, 'Registro não inserido - Dados incorretos!', $exception, 406);
        }
    }

    public function update(Request $request, $table, $id)
    {
        $model = "App\\Models\\" . $table;
        $payload = $request->all();
        try {
            $response = $model::find($id);
            if (!$response)
                return Handles::jsonResponse(false, 'Registro não encontrado.', $response, 406);
            $response->fill($payload);
            return $response->save()
                ?   Handles::jsonResponse(true, 'Registro ' . $response->getKey() . ' atualizado.', $response, 200)
                :   Handles::jsonResponse(false, 'Falha ao inserir.', $response, 406);
        } catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse(false, 'Registro não atualizado, verifique os campos.', $exception, 406);
        }
    }

    public function destroy($table, $id)
    {
        $model = "App\\Models\\" . $table;
        try {
            $response = $model::find($id);
            if (!$response)
                return Handles::jsonResponse(false, 'Registro não encontrado.', $response, 406);
            return $response->delete()
                ?   Handles::jsonResponse(true, 'Registro excluído.', [], 204) // No data
                :   Handles::jsonResponse(false, 'Falha ao excluir.', [], 406);
        } catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse(false, 'Falha ao excluir.', $exception, 406);
        }
    }
}
